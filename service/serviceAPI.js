axios.defaults.baseURL = "https://64c62b5ec853c26efadb28e8.mockapi.io";
// axios.defaults.headers.post['Content-Type'] = 'application/json';
// axios.defaults.headers = { 'Content-Type': 'application/json' };

let getData = async (path) => {
    let res = await axios(path, { method: 'GET' });
    return res.data;
}

let deleteData = async (path, ma) => {
    return await axios(path + ma, { method: "DELETE" })
}

let postData = async (path, data) => {
    return await axios(path, { method: "POST", data });
}

let putData = async (path, option = {}) => {
    return await axios.put(path, option)
}


let service = {
    getData,
    deleteData,
    postData,
    putData,
};

export default service;