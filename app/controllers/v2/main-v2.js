import * as Food from "../../models/v2/Food.js";
import service from "../../../service/serviceAPI.js";
import Alert from "../../../ulti/alert.js";
import ClearForm from "../../../ulti/resetForm.js";



let RenderTable = (data) => {
    const tbodyFood = document.getElementById('tbodyFood');
    data = data.reverse()
    let text = "";
    tbodyFood.innerHTML = "";
    data.forEach(element => {
        let food = new Food.Food(
            element.ma,
            element.ten,
            element.gia,
            element.loai,
            element.khuyenMai,
            element.tinhTrang,
            element.moTa,
            element.hinhAnh,
        );

        text += `
                <tr>
                    <td>${food.ma}</td>
                    <td><img src="${food.hinhAnh}" />${food.ten}</td>
                    <td>${food.loai}</td>
                    <td>${food.gia}</td>
                    <td>${food.khuyenMai}%</td>
                    <td>${food.GiaKhuyenMai()}</td>
                    <td>${food.TinhTrang()}</td>
                    <td>
                        <button onclick="DeleteFood(${food.ma})" class="btn btn-danger">Delete</button>
                        <button onclick="EditFood(${food.ma})" class="btn btn-warning">Edit</button>
                    </td>
                </tr > `
    });
    tbodyFood.innerHTML = text;
}

window.DeleteFood = async (ma) => {
    try {
        await service.deleteData('/food/', ma)
            .then(async (res) => {
                Alert.sucess(res.statusText);
                SlectLoaiChange();
            }).catch((err) => {
                Alert.error(err);
                throw err;
            })
    }
    catch (err) {
        console.error(err);
    }
}

window.EditFood = async (id) => {
    let { ma, ten, gia, loai, khuyenMai, tinhTrang, moTa, hinhAnh } = await service.getData("/food/" + id);

    document.getElementById('foodID').value = ma;
    document.getElementById('tenMon').value = ten;
    document.getElementById('loai').value = (loai) ? "loai2" : "loai1";
    document.getElementById('giaMon').value = gia;
    document.getElementById('khuyenMai').value = (khuyenMai) ? '20' : '10';
    document.getElementById('tinhTrang').value = (tinhTrang) ? '1' : '0';
    document.getElementById('hinhMon').value = hinhAnh;
    document.getElementById('moTa').value = moTa;
    document.getElementById('foodID').setAttribute('readonly', "");
    // Open Modal 
    $('#exampleModal').modal('show')
}


document.querySelector('#btnCapNhat').onclick = async () => {
    event.preventDefault();
    try {
        let data = Food.CreateFood().ChangeData();
        await service.putData(`/food/${data.ma}/`, data)
            .then(async (res) => {
                Alert.sucess(res.statusText);
                ClearForm();
                $('#exampleModal').modal('hide') // Hide Modal
                SlectLoaiChange();
            }).catch((err) => {
                Alert.error(err);
                throw err;
            })
    }
    catch (err) {
        console.error(err);
    }
}

document.querySelector('#btnThemMon').onclick = async () => {
    event.preventDefault();
    document.getElementById('foodID').removeAttribute('readonly');
    try {
        let data = Food.CreateFood().ChangeData();
        await service.postData('/food/', data)
            .then(async (res) => {
                Alert.sucess(res.statusText);
                ClearForm();
                SlectLoaiChange();
            }).catch((err) => {
                Alert.error(err);
                throw err;
            });
    }
    catch (err) {
        console.error(err);
    }
}

var SlectLoaiChange = document.querySelector('#selLoai').onchange = async () => {
    const selLoai = document.querySelector('#selLoai').value;
    let data = await SearchLoaiFood(selLoai);
    RenderTable(data);
}

let SearchLoaiFood = async (selLoai) => {
    let data = [];
    data = await service.getData("/food/");

    switch (selLoai) {
        case "all": data.filter(() => true); break;
        case "loai1": data = data.filter((pre) => pre.loai == false); break;
        case "loai2": data = data.filter((pre) => pre.loai == true); break;
        default:
            data.filter(() => true); break;
    }
    return data;
}

document.body.onload = async () => {
    let data = await service.getData('/food/');
    RenderTable(data);
}


