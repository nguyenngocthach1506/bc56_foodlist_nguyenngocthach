import Validation from "../../../ulti/Validation.js";
var valid = true;
//loai: true => Mặn | false => Chay
//tinhTrang: true=> Còn | false => Hết
//khuyenMai : true => 20% | false => 10%

export class Food {
    constructor(ma, ten, gia, loai, khuyenMai, tinhTrang, moTa, hinhAnh) {
        this.ma = ma;
        this.ten = ten;
        this.gia = gia;
        this.loai = (loai) ? "Mặn" : "Chay";
        this.khuyenMai = (khuyenMai) ? 20 : 10;
        this.tinhTrang = (tinhTrang) ? "Còn" : "Hết";
        this.moTa = moTa;
        this.hinhAnh = hinhAnh;
    }

    ChangeData = () => {
        return {
            ma: this.ma,
            ten: this.ten,
            gia: this.gia,
            loai: (this.loai == "Mặn") ? true : false,
            khuyenMai: (this.khuyenMai == "20") ? true : false,
            tinhTrang: (this.tinhTrang == "Còn") ? true : false,
            moTa: this.moTa,
            hinhAnh: this.hinhAnh,
        };
    }
    GiaKhuyenMai = () => {
        return (this.gia * (100 - this.khuyenMai) / 100);
    }
    TinhTrang = () => {
        return (this.tinhTrang == "Còn") ? '<span class="text-success">Còn</span>' : '<span class="text-danger">Hết</span>'
    }
}

/*  */
const invalidID = document.getElementById('invalidID');
const invalidTen = document.getElementById('invalidTen');
const invalidLoai = document.getElementById('invalidLoai');
const invalidGia = document.getElementById('invalidGia');
const invalidKM = document.getElementById('invalidKM');
const invalidTT = document.getElementById('invalidTT');
const invalidhinhMon = document.getElementById('invalidhinhMon');
const invalidMoTa = document.getElementById('invalidMoTa');
/* */
export let CreateFood = () => {
    /* */
    const FOODID = document.getElementById('foodID').value;
    const TENMON = document.getElementById('tenMon').value;
    const LOAI = document.getElementById('loai');
    const GIAMON = document.getElementById('giaMon').value;
    const KHUYENMAI = document.getElementById('khuyenMai');
    const TINHTRANG = document.getElementById('tinhTrang');
    const HINHMON = document.getElementById('hinhMon').value;
    const MOTA = document.getElementById('moTa').value;
    console.log(KHUYENMAI);
    ClearValid();
    /* */
    valid = Validation.EmptyValid(invalidID, FOODID)
        & Validation.EmptyValid(invalidTen, TENMON)
        & Validation.EmptyValid(invalidGia, GIAMON)
        & Validation.EmptyValid(invalidhinhMon, HINHMON)
        & Validation.EmptyValid(invalidMoTa, MOTA);

    valid &= Validation.KiemTraNaN(invalidGia, GIAMON);

    valid &= Validation.KiemTraSelectValid(invalidLoai, LOAI.options.selectedIndex)
        & Validation.KiemTraSelectValid(invalidTT, TINHTRANG.options.selectedIndex)
        & Validation.KiemTraSelectValid(invalidKM, KHUYENMAI.options.selectedIndex);
    setTimeout(ClearValid, 5000);
    if (valid) {
        let km = (KHUYENMAI.value == 20) ? true : false;
        let tt = (TINHTRANG.value == "1") ? true : false;
        let loai = (LOAI.value == "loai2") ? true : false;
        return new Food(FOODID,
            TENMON, GIAMON,
            loai,
            km, tt,
            MOTA,
            HINHMON);
    }
}

function ClearValid() {
    invalidID.innerHTML = "";
    invalidTen.innerHTML = "";
    invalidLoai.innerHTML = "";
    invalidGia.innerHTML = "";
    invalidKM.innerHTML = "";
    invalidTT.innerHTML = "";
    invalidhinhMon.innerHTML = "";
    invalidMoTa.innerHTML = "";
}