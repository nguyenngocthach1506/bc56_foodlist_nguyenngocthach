let sucess = (mess) => {
    Swal.fire(
        mess,
        'You clicked the button!',
        'success'
    )
}

let error = (mess) => {
    Swal.fire({
        icon: 'error',
        text: mess,
        text: 'Something went wrong!',
    })
}

let Alert = {
    sucess,
    error,
}

export default Alert;