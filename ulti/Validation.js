
/*============================================================= */


// False: Không hợp lệ
// True: Hợp lệ


/* Kiểm tra chuỗi trống */
function EmptyValid(object, string, error = "Không được trống !!!") {
    /* */
    if (string.trim() == "") {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Kiểm tra chỉ chứa ký chữ thường hoặc chữ Hoa */
function KiemTraHoTenValid(object, value, error = "Họ tên phải là chữ!") {
    /* */
    var regex = /^[a-zA-Z]+$/;
    value = ChuyenThanhKhongDau(value);
    if (!regex.test(value)) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Kiếm tra chuỗi từ minlength đến maxlength */
function KiemTraKySoValid(object, value, error = "Nhập số từ 4-6 ký số") {
    /* */
    var regex = /^[0-9]{4,6}$/g;
    if (!regex.test(value)) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Kiếm tra số từ minlength đến maxlength */
function KiemTraDoDaiValid(object, value, minlength, maxlength
    , error = `Từ ${minlength.toLocaleString()} đến ${maxlength.toLocaleString()} !!!`) {
    /* */
    if (value < minlength || value > maxlength || isNaN(value)) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Kiểm tra Not a Number(NaN) */
function KiemTraNaN(object, value, error = "Không phải số") {
    /* */
    if (isNaN(value)) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Kiểm tra chức vụ */
function KiemTraSelectValid(object, index, error = "Chọn không hơp lệ") {
    /* */
    if (index <= 0) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Kiểm tra Email */
function KiemTraEmailValid(object, value, error = "Email không hợp lệ") {
    /* */
    var regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regexEmail.test(value)) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/*
    Kiểm tra Mật Khẩu từ 6-10 ký tự
    - chứa  chữ HOA, chữ thường
    - ít nhất 1 ký tự đặc biệt
*/
function KiemTraMatKhauValid(object, value, error = "Mật khẩu không hợp lệ: <br>-từ 6-10 ký tự <br>-chứa chữ Hoa, chữ thường, <br>-ít nhất 1 ký tự đặc biệt") {
    /* */
    var regex = /^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,10}$/g;
    if (!regex.test(value)) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Kiểm tra Ngày theo định đạng mm/dd/yyyy */
function KiemTraNgayLamValid(object, value, error = "Ngày làm định dạng mm/dd/yyyy") {
    /* */
    var regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if (!regex.test(value)) {
        object.style.display = "block";
        object.innerHTML += `<span style="display:block;">${error}<span>`;
        return false;
    }
    return true;
}

/* Đổi ký tự có dấu thành không dấu */
function ChuyenThanhKhongDau(title) {
    title = title.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    title = title.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    title = title.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    title = title.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    title = title.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    title = title.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    title = title.replace(/đ/gi, 'd');

    return title;
}


let Valid = {
    EmptyValid,
    KiemTraNaN,
    KiemTraSelectValid,
}

export default Valid;

