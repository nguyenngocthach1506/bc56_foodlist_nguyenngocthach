let ClearForm = () => {
    document.getElementById('foodID').value = "";
    document.getElementById('tenMon').value = "";
    document.getElementById('loai').options.selectedIndex = 0;
    document.getElementById('giaMon').value = "";
    document.getElementById('khuyenMai').options.selectedIndex = 0;
    document.getElementById('tinhTrang').options.selectedIndex = 0;
    document.getElementById('hinhMon').value = "";
    document.getElementById('moTa').value = "";
    document.getElementById('foodID').removeAttribute('readonly');
}


export default ClearForm;